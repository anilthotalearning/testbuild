FROM openjdk:11
ADD target/TestBuild-1.0.jar TestBuild-1.0.jar
EXPOSE 9080
ENTRYPOINT ["java", "-jar", "TestBuild-1.0.jar"]